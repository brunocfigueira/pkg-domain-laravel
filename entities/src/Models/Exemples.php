<?php

namespace Bcfigueira\Domain\Entities\Models;

use Illuminate\Database\Eloquent\Model;

class Exemples extends Model
{
    /**
     * @var string
     */
    protected $table = 'exemples';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];
}
